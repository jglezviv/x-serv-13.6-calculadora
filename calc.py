def sumar(num1, num2):
    resultado = num1 + num2
    print("la suma de ", num1, "y", num2, "tiene como resultado: ", resultado)


def restar(num1, num2):
    resultado = num1 - num2
    print("la resta de ", num1, "y", num2, "tiene como resultado: ", resultado)

# sumas
sumar(1, 2)
sumar(3, 4)
# restas
restar(5, 6)
restar(9, 8)
